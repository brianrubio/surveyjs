import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthProvider } from '../../providers/auth/auth';

/**
 * Generated class for the CerrarSesionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cerrar-sesion',
  templateUrl: 'cerrar-sesion.html',
})
export class CerrarSesionPage {

  constructor(public navCtrl: NavController,public auth: AuthProvider,public afA: AngularFireAuth,private alertCtrl: AlertController, public navParams: NavParams,public alerta: AlertController) {
  }
 

qAlert() {
  let alert = this.alertCtrl.create({
    title: "¿Seguro que desea Cerrar Sesión?",
    buttons: ['Aceptar']
  });
  alert.present();
}

advertencia(){
  let Alerta = this.alertCtrl.create({
    title: "¿Esta seeguro que desea salir?",
    buttons: [
      {
        text: 'Cancelar',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Aceptar',
        handler: data => {
          this.auth.logout();
        }
      }
    ]
  });
  Alerta.present();

}

}
