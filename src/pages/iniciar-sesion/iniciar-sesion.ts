import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { ToastController } from 'ionic-angular';
import { TabsAdminPage } from '../tabs-admin/tabs-admin';

/**
 * Generated class for the IniciarSesionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-iniciar-sesion',
  templateUrl: 'iniciar-sesion.html',
})
export class IniciarSesionPage {
  
  user = { email: '', password: '' };
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    public alertCtrl: AlertController,
    private toastCtrl: ToastController

  ) {


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IniciarSesionPage');
  }
  //Registro de Usuario
  SignUp() {
    this.auth.registerUser(this.user.email, this.user.password)
      .then((user) => {
        // El usuario se ha creado correctamente
        this.presentToast();
      })
      .catch(err => {
        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: err.message,
          buttons: ['Aceptar']
        });
        alert.present();
      })

  }

  Login() {
    const result = this.auth.loginUser(this.user.email, this.user.password).then((user) => {
      if (result) {
        this.navCtrl.setRoot(TabsAdminPage);
      }
      // Inicio de sesión correcta
    })
      .catch(err => {
        console.log(err.message);

        if (err.message =="The email address is badly formatted.") {
          let alert = this.alertCtrl.create({
            title: 'El correo electrónico ingresado es incorrecto',
            buttons: ['Aceptar']
          });
          alert.present();
        }
        else if (err.message =="The password is invalid or the user does not have a password.") {
          let alert = this.alertCtrl.create({
            title: 'La contraseña es incorrecta',
            buttons: ['Aceptar']
          });
          alert.present();
        }
        else if (err.message =="There is no user record corresponding to this identifier. The user may have been deleted.") {
          let alert = this.alertCtrl.create({
            title: 'El usuario ingresado no existe',
            buttons: ['Aceptar']
          });
          alert.present();
        }
        
        else{
          let alert = this.alertCtrl.create({
            title: err.message,
            buttons: ['Aceptar']
          });
          alert.present();

        }
      })
  }

  //Mensaje emergente
  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Usuario registrado exitosamente.',
      duration: 3000,
      position: 'middle'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

}
