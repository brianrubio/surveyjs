import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController, ItemSliding, ToastController } from 'ionic-angular';

import { SurveyProvider } from '../../providers/survey/survey';
import { SurveyDetailsPage } from '../survey-details/survey-details';

import { SurveyModel } from "../../models/survey.model";
import { AuthProvider } from '../../providers/auth/auth';
import { ApiWrapper } from '../../providers/survey/api-wrapper';
import {Observable} from 'rxjs/Observable';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import 'rxjs/add/observable/forkJoin';
//Referencias a paginas adiacentes
import { 
    IniciarSesionPage,
    SurveyResultsPage,
    ResultadosPage,

    GraficasDesgastePage,
    GraficasClimaPage,
    GraficasResponsabilidadPage,
    GraficasSatisfaccionPage 
} from '../index.paginas';

@Component({
    selector: 'page-home1a',
    templateUrl: 'home1a.html'
})
export class Home1aPage {

    surveys: SurveyModel[];
    archiveSurveys: SurveyModel[];
    defaultImages: any;
    noSurveys: boolean = false;
    currentYear = new Date().getFullYear();
    allId: String[] = [];

    constructor(public navCtrl: NavController, 
        public surveyProvider: SurveyProvider,
        public loadingCtrl: LoadingController, 
        public alertCtrl: AlertController, 
        public apiWrapper: ApiWrapper,
        public auth: AuthProvider,
        public deleteResult:SurveyProvider,
        private toastCtrl: ToastController,
        private iab: InAppBrowser
            ) {
        this.allId.push('fc724d10-65a0-4e44-bd79-38e9d931d55f');
        this.allId.push('2c071c0c-1b05-4553-8bd0-318417ce3a7d');
        this.allId.push('32cf120a-0089-4fe3-8111-4bb17bf78e2a');
        this.allId.push('0c0edb58-c798-4b0d-8e58-01f0af0d59d1');
        //this.getActiveSurveys();
        //this.getArchiveSurveys();
        this.getSurveys();


        // TO TEST API WRAPPER UNCOMMENT THIS CODE. 
        /*
        this.apiWrapper.api.surveys.get('getActive', { accessKey: true, ownerId: true }).subscribe(
            data => {
                console.log(data);
            },
            error => {
                console.log(<any>error);
            }
        );
        */
 
    }
    CerrarSesion(){

        let Alerta = this.alertCtrl.create({
            title: "¿Está seguro que desea Cerrar Sesión?",
            buttons: [
                {
                    text: 'Cancelar',
                    handler: data => {

                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Aceptar',
                    handler: data => {
                        //  window.location.reload(); 
                        //this.app.getRootNav().setRoot(TabsAdminPage);        
                        this.auth.logout();
                    }
                }
            ]
        });
        Alerta.present();

   }


    getSurveys() {
        let loading = this.loadingCtrl.create({
            content: "Cargando..."
        });
        loading.present();
        Observable.forkJoin(this.surveyProvider.getActiveSurveys(), this.surveyProvider.getArchiveSurveys())
            .subscribe(data => {
                //console.log(data);
                this.surveys = SurveyModel.fromJSONArray(data[0]);
                this.archiveSurveys = SurveyModel.fromJSONArray(data[1]);
                loading.dismiss();
            },
            error => {
                console.log(<any>error);
                if ((error.message == "Failed to get surveys.") || (error.message == "Http failure response for (unknown url): 0 Unknown Error")) this.noSurveys = true;
                loading.dismiss();
            });
    }

    getActiveSurveys() {
        let loading = this.loadingCtrl.create({
            content: "Cargando..."
        });

        loading.present();

        this.surveyProvider.getActiveSurveys()
            .subscribe(
                data => {
                    //console.log(data);
                    //this.surveys = data;
                    this.surveys = SurveyModel.fromJSONArray(data);
                    loading.dismiss();
                },
                error => {
                    console.log(<any>error);
                    if ((error.message == "Failed to get surveys.") || (error.message == "Http failure response for (unknown url): 0 Unknown Error")) this.noSurveys = true;
                    loading.dismiss();
            }
        );
    }

    getArchiveSurveys() {
        this.surveyProvider.getArchiveSurveys()
            .subscribe(
                data => {
                    //console.log(data);
                    this.archiveSurveys = SurveyModel.fromJSONArray(data);
                },
                error => {
                    console.log(<any>error);
                    if ((error.message == "Failed to get surveys.") || (error.message == "Http failure response for (unknown url): 0 Unknown Error")) this.noSurveys = true;
            }
        );
    }

    selectedSurvey(survey) {
       console.log("Survey: "+survey.Id);
    //Desgaste GraficasDesgastePage
        if (survey.Id == "fc724d10-65a0-4e44-bd79-38e9d931d55f") {
            this.navCtrl.push(GraficasDesgastePage, {
                survey: survey
            });
        }
        //Clima laboral
        else if (survey.Id == "2c071c0c-1b05-4553-8bd0-318417ce3a7d") {
            this.navCtrl.push(GraficasClimaPage, {
                survey: survey
            });
        }
        //Responsabilidad Social
        else if (survey.Id == "32cf120a-0089-4fe3-8111-4bb17bf78e2a") {
            this.navCtrl.push(GraficasResponsabilidadPage, {
                survey: survey
            });
        }
        //Satisfacción Laboral
        else {
            this.navCtrl.push(GraficasSatisfaccionPage, {
                survey: survey
            });
        }
        ///////       
    }

    presentAlert({
        survey = null,
        operation = '', 
      } = {}) {
        let options = this.alertConfig(operation);
        let alert = this.alertCtrl.create({
          title: options.title,
          subTitle: options.subTitle,
          buttons: [
            {
                text: 'Cancel',
                handler: () => {
                }
            },
            {
              text: 'Accept',
              handler: () => {
                if (operation == 'delete') this.deleteSurvey(survey);
                if (operation == 'activate') this.activateSurvey(survey);
                if (operation == 'archive') this.archiveSurvey(survey);
                if (operation == "create") this.createSurvey("New Survey :)");
              }
            }
          ]
        });
        alert.present();
    }

    showPrompt(survey, slidingItem: ItemSliding) {
        let prompt = this.alertCtrl.create({
          title: 'Update Survey Name',
          message: "Enter a name for this survey",
          inputs: [
            {
              name: 'name',
              placeholder: 'Name'
            },
          ],
          buttons: [
            {
              text: 'Cancel',
              handler: data => {
                //console.log('Cancel clicked');
              }
            },
            {
              text: 'Accept',
              handler: data => {
                //console.log('Accept clicked');
                //console.log(data);
                this.changeSurveyName(survey, data.name, slidingItem);
              }
            }
          ]
        });
        prompt.present();
    }

    createSurvey(name) {
        let loading = this.loadingCtrl.create({
            content: "Creating Survey..."
        });

        loading.present();

        this.surveyProvider.createSurvey(name)
        .subscribe(
            data => {
                //console.log(data);
                let survey: SurveyModel = new SurveyModel(data);
                this.surveys.unshift(survey);
                loading.dismiss();
            },
            error => {
                console.log(<any>error);
                loading.dismiss();
            }
        );
    }


    deleteSurvey(survey) {
        let loading = this.loadingCtrl.create({
            content: "Deleting survey"
        });

        loading.present();

        this.surveyProvider.deleteSurvey(survey.Id)
        .subscribe(
            data => {
                console.log(data);
                loading.dismiss();
            },
            error => {
                console.log(<any>error);
                if (error.status == 200) {
                    if ( survey.IsArchived === false) this.surveys = this.removeElement(survey.Id, this.surveys);
                    else this.archiveSurveys = this.removeElement(survey.Id, this.archiveSurveys);
                }
                loading.dismiss();
            }
        );
    }

    changeSurveyName(survey, newName, slidingItem) {
        let loading = this.loadingCtrl.create({
            content: "Updating Survey name..."
        });

        loading.present();

        this.surveyProvider.changeSurveyName(survey.Id, newName)
        .subscribe(
            data => {
                console.log(data);
                slidingItem.close();
                loading.dismiss();
            },
            error => {
                console.log(<any>error);
                if (error.status == 200)  {
                    survey.Name = newName;
                    slidingItem.close();
                }
                loading.dismiss();
            }
        );
    }

    activateSurvey(survey) {
        let loading = this.loadingCtrl.create({
            content: "Activating Survey..."
        });

        loading.present();

        this.surveyProvider.restoreSurvey(survey.Id)
        .subscribe(
            data => {
                console.log(data);
                loading.dismiss();
            },
            error => {
                console.log(<any>error);
                if (error.status == 200) {
                    this.surveys.push(survey);
                    this.archiveSurveys = this.removeElement(survey.Id, this.archiveSurveys);
                }
                loading.dismiss();
            }
        );
    }

    archiveSurvey(survey) {
        let loading = this.loadingCtrl.create({
            content: "Archiving Survey..."
        });

        loading.present();

        this.surveyProvider.archiveSurvey(survey.Id)
        .subscribe(
            data => {
                console.log(data);
                loading.dismiss();
            },
            error => {
                console.log(<any>error);
                if (error.status == 200) {
                    this.archiveSurveys.push(survey);
                    this.surveys = this.removeElement(survey.Id, this.surveys);
                }
                loading.dismiss();
            }
        );
    }

    removeElement(surveyId, surveys) {
        return surveys.filter(function(e) {
            return e.Id !== surveyId;
        });
    }

    alertConfig(operation) {
        let options = {
            delete: {title: 'Delete Survey', subTitle: '¿Are you sure to delete the survey?'},
            activate: {title: 'Activate Survey', subTitle: '¿Are you sure to activate the survey?'},
            archive: {title: 'Archive Survey', subTitle: '¿Are you sure to archive the survey?'},
            create: {title: 'Create Survey', subTitle: '¿Are you sure to create new survey?'}

        }
        return options[operation];
    }

    openLogin(){
        this.navCtrl.push(IniciarSesionPage);
    }

    warninigDeleteAllResults(){
		let Alerta = this.alertCtrl.create({
		  title: "¿Esta seguro de borrar todos los resultados de las encuestas?",
		  buttons: [
			{
			  text: 'Cancelar',
			  handler: data => {
				console.log('Cancel clicked');
			  }
			},
			{
			  text: 'Aceptar',
			  handler: data => {
				this.onClickDeleteSurveyResult();
			  }
			}
		  ]
		});
        Alerta.present();
	  
      }
      
      onClickDeleteSurveyResult() {
		let loading = this.loadingCtrl.create({
            content: "Eliminando datos de todas las encuestas."
        });

        loading.present().then(()=>{
            for(let i=0; i < this.allId.length; i++){
                this.surveyProvider.deleteAllSurveyResult(this.allId[i])
            
                    .subscribe(
                        data => {
                        console.log(data);
                    },
                    error => {
                    console.log(<any>error);
                    }
                );
            }
            loading.dismiss(); 
            this.iab.create("https://surveyjs.io/Results/ExportResults/fc724d10-65a0-4e44-bd79-38e9d931d55f?format=xls", "_system");
            this.iab.create("https://surveyjs.io/Results/ExportResults/2c071c0c-1b05-4553-8bd0-318417ce3a7d?format=xls", "_system");
            this.iab.create("https://surveyjs.io/Results/ExportResults/0c0edb58-c798-4b0d-8e58-01f0af0d59d1?format=xls", "_system");
            this.iab.create("https://surveyjs.io/Results/ExportResults/32cf120a-0089-4fe3-8111-4bb17bf78e2a?format=xls", "_system");
            
            
            this.presentToast();

        });    
	}
    
    presentToast() {
        let toast = this.toastCtrl.create({
            message: 'Los registros de todas las encuestas han sido eliminados',
            duration: 3000,
            position: 'middle'
        });

        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });

        toast.present();
    }

}
