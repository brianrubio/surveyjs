import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GraficaSemaforoPage } from './grafica-semaforo';

@NgModule({
  declarations: [
    GraficaSemaforoPage,
  ],
  imports: [
    IonicPageModule.forChild(GraficaSemaforoPage),
  ],
})
export class GraficaSemaforoPageModule {}
