//Exportador/Importador de Referencias a todas las paginas de la aplicacion

export { IniciarSesionPage } from './iniciar-sesion/iniciar-sesion';
export { ResultadosPage } from './resultados/resultados';
export { SurveyResultsPage } from './survey-results/survey-results';
export { SurveyResultsModel } from './../models/survey.results.model';
//Graficas
export { GraficasClimaPage } from './graficas-clima/graficas-clima';
export { GraficasDesgastePage } from './graficas-desgaste/graficas-desgaste';
export { GraficasResponsabilidadPage } from './graficas-responsabilidad/graficas-responsabilidad';
export { GraficasSatisfaccionPage } from './graficas-satisfaccion/graficas-satisfaccion';
