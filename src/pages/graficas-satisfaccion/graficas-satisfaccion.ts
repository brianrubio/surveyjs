import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ModalController,
  AlertController,
  ToastController,
  FabContainer
} from 'ionic-angular';

//Importaciones extra
import { SurveyProvider } from '../../providers/survey/survey';
import { ChartsModalPage } from '../../modals/charts-modal';
import { SurveyResultsModel } from '../../models/survey.results.model';

import * as papa from 'papaparse';
import { Observable } from 'rxjs/Observable';

import { Screenshot } from '@ionic-native/screenshot';
import { NativeStorage } from '@ionic-native/native-storage';

declare var google;
declare var AmCharts;

@IonicPage()
@Component({
  selector: 'page-graficas-satisfaccion',
  templateUrl: 'graficas-satisfaccion.html',
})
export class GraficasSatisfaccionPage {
  screen: any;
  state: boolean = false;


  number_satisf: any;

  //resultados individuales por defecto
  resulEncuestas: string = "individual";

  //Variables locales para resultados
  currentYear = new Date().getFullYear();
  result: Observable<any>;
  survey: any;
  keys: any;
  surveyResults: SurveyResultsModel[] = [];

  //Arreglo auxiliar
  auxArray: any = [];

  //Arreglo de resultados obtenidos
  public satisfaccionValues: any = [];

  //Variables de Cálculos
  nuEmpleado: any;
  colorSema: any;

  //valores de semaforo generales
	rojo: number = 0;
	verde: number = 0;
  amarillo: number = 0;
  sumD13 = 0;
  sumD12 = 0;
  sumD11 = 0;

  //Variables para respuestas
  p1: any; p2: any; p3: any; p4: any; p5: any; p6: any; p7: any; p8: any; p9: any; p10: any;
  p11: any; p12: any; p13: any; p14: any; p15: any; p16: any; p17: any; p18: any;

  //Valores de radar
  sumlvl1: number;
  sumlvl2: number;
  sumlvl3: number;
  sumlvl4: number;
  sumlvl5: number;
  sumlvl6: number;

  



  //Constructor de la página
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public surveyProvider: SurveyProvider,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private screenshot: Screenshot,
    private nativeStorage: NativeStorage
  ) {
    //this.result = this.navParams.get('result');
    //this.CalcularSemaforo();

    this.survey = this.navParams.get('survey');
    this.survey.publicSurveyURL = 'https://surveyjs.io/Results/Survey/' + this.survey.Id;
    //console.log("id->" + this.survey.Id)

    let loading = this.loadingCtrl.create({
      content: "Cargando resultados..."
    });

    loading.present();

    this.surveyProvider.getSurveyResults(this.survey.Id)
      .subscribe(
        data => {
          this.surveyResults = SurveyResultsModel.fromJSONArray(data.Data);
          if(this.surveyResults[0]){
            this.keys = this.surveyResults[0].userAnswers.map((val, key) => { return val['textQuestion'] });
          }else{
            let Alerta = this.alertCtrl.create({
              title: "Esta encuesta aún no cuenta con registros.",
              buttons: [
              {
                text: 'Aceptar',
                handler: data => {
                  this.navCtrl.pop();
                }
              }
              ]
            });
            Alerta.present();
          }
          
          nativeStorage.getItem('storage_satisf').then(
            data => this.number_satisf = data,
            error => this.nativeStorage.setItem('storage_satisf', 0)
          );

          //DATAS HERE
          // console.log("***RESULTS:",this.surveyResults);

          for (let a = 0; a <= this.surveyResults.length - 1; a++) {
            this.nuEmpleado = (this.surveyResults[a].userAnswers[0].value);

            this.p1 = this.surveyResults[a].userAnswers[5].value;
            this.p2 = this.surveyResults[a].userAnswers[6].value;
            this.p3 = this.surveyResults[a].userAnswers[7].value;
            this.p4 = this.surveyResults[a].userAnswers[8].value;
            this.p5 = this.surveyResults[a].userAnswers[9].value;
            this.p6 = this.surveyResults[a].userAnswers[10].value;
            this.p7 = this.surveyResults[a].userAnswers[11].value;
            this.p8 = this.surveyResults[a].userAnswers[12].value;
            this.p9 = this.surveyResults[a].userAnswers[13].value;

            this.p10 = this.surveyResults[a].userAnswers[14].value;
            this.p11 = this.surveyResults[a].userAnswers[15].value;
            this.p12 = this.surveyResults[a].userAnswers[16].value;
            this.p13 = this.surveyResults[a].userAnswers[17].value;

            this.p14 = this.surveyResults[a].userAnswers[18].value;
            this.p15 = this.surveyResults[a].userAnswers[19].value;
            this.p16 = this.surveyResults[a].userAnswers[20].value;
            this.p17 = this.surveyResults[a].userAnswers[21].value;
            this.p18 = this.surveyResults[a].userAnswers[22].value;

            this.CalcularSemaforo();

            //contadores del semaforo
					if (this.colorSema >= 0 && this.colorSema < 2.5 ){
            this.rojo =(this.rojo *1 )+ 1; 
            } else if (this.colorSema >= 2.5 && this.colorSema < 3.3 ){
            this.amarillo = (this.amarillo * 1) + 1;
            } else if (this.colorSema >= 3.3 && this.colorSema <= 5 ){
            this.verde = (this.verde * 1) + 1;             
            }

            //Limpiar Arreglo Auxiliar
            this.auxArray = [];

            //Creación de Arreglo
            //Datos personales
            this.auxArray.push(this.nuEmpleado);
            //Edad
            this.auxArray.push(this.surveyResults[a].userAnswers[1].value);
            //Estado Civil
            this.auxArray.push(this.surveyResults[a].userAnswers[2].value);
            //Género
            this.auxArray.push(this.surveyResults[a].userAnswers[3].value);
            //Antiguedad
            this.auxArray.push(this.surveyResults[a].userAnswers[4].value);

            //Semáforo
            this.auxArray.push(this.colorSema);

            //Completar arreglo
            this.satisfaccionValues.push(this.auxArray)

          }

          console.log(this.auxArray);
          console.log(this.satisfaccionValues);
          loading.dismiss();

        }, error => {
          console.log(<any>error);
          loading.dismiss();
        }
      );

    this.result = this.satisfaccionValues;

  }

  CalcularSemaforo() {
    this.colorSema = (this.p1 * 1 + this.p2 * 1 + this.p3 * 1 + this.p4 * 1 + this.p5 * 1 +
      this.p6 * 1 + this.p7 * 1 + this.p8 * 1 + this.p9 * 1 + this.p10 * 1 +
      this.p11 * 1 + this.p12 * 1 + this.p13 * 1 + this.p14 * 1 + this.p15 * 1 +
      this.p16 * 1 + this.p17 * 1 + this.p18 * 1) / 18;
  }

  ngOnInit() {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GraficasSatisfaccionPage');
  }

  //Funciones de muestreo de resultados

  openDetails(result) {

    this.navCtrl.push('ResultadosSatisfaccionPage', { result: result });

  }


  formatChartData() {
    const results = this.surveyResults;
    let userAnswers = [];
    // Concatenate all user's answers.
    results.map(result => {
      userAnswers.push(...result.userAnswers)
    });

    // Group by answers by question.
    let data = userAnswers.reduce(function (rv, x) {
      (rv[x['idQuestion']] = rv[x['idQuestion']] || []).push(x.value);
      return rv;
    }, {});

    const chartData = Object.keys(data).map(i => data[i]);

    return chartData;
  }

  openModal() {
    let modal = this.modalCtrl.create(ChartsModalPage, { 'chartData': this.formatChartData(), 'questionsText': this.keys });
    modal.present();
  }

  downloadResults(result) {
    //console.log("downloadResults");********

    let csv = papa.unparse({
      fields: this.keys,
      data: this.formatChartData()
    });
    // console.log(this.keys);
    //console.log(this.formatChartData());

    // Dummy implementation for Desktop download purpose.
    // let blob = new Blob([csv]);
    // Sent the UTF-8 header for the download process.
    let blob = new Blob(["\ufeff", csv]);
    let a = window.document.createElement("a");
    //create page
    a.href = window.URL.createObjectURL(blob);


    a.download = "desgaste-ocupacional.csv";
    //b.download = "desgaste-ocupacional.csv";		


    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }

  makeSurveyResultsPublic(content) {
    //console.log("makeSurveyResultsPublic");
    this.survey.AllowAccessResult = !this.survey.AllowAccessResult;

    let loading = this.loadingCtrl.create({
      content: content
    });
    loading.present();
    this.surveyProvider.makeSurveyResultsPublic(this.survey.Id, this.survey.AllowAccessResult)
      .subscribe(
        data => {
          // console.log(data);
          loading.dismiss();
        },
        error => {
          // console.log(<any>error);
          loading.dismiss();
        }
      );
  }

  warninigDeleteAllResults(event, fab: FabContainer){
    fab.close();
    let Alerta = this.alertCtrl.create({
      title: "¿Está seguro de borrar todos los registros de la encuesta Satisfacción Laboral?",
      buttons: [
      {
        text: 'Cancelar',
        handler: data => {
        console.log('Cancel clicked');
        }
      },
      {
        text: 'Aceptar',
        handler: data => {
        this.onClickDeleteSurveyResult();
        }
      }
      ]
    });
    Alerta.present();
  }
  
      onClickDeleteSurveyResult() {
        let loading = this.loadingCtrl.create({
                content: "Eliminando las respuestas de todas las encuestas."
            });
    
            loading.present().then(()=>{
                    this.surveyProvider.deleteAllSurveyResult(this.survey.Id)
                        .subscribe(
                            data => {
                            console.log(data);
                        },
                        error => {
                        console.log(<any>error);
                        }
                    );
                loading.dismiss();   
            }
          );  
          this.navCtrl.pop(); 
        this.presentToast();
      }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Los registros de la encuesta Satisfacción Laboral han sido eliminados',
      duration: 3000,
      position: 'middle'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  presentAlert() {
    let operation;
    let loadingContent;
    if (this.survey.AllowAccessResult) {
      operation = "disable";
      loadingContent = "Making Survey results not public..."
    }
    else {
      operation = "grant";
      loadingContent = "Making Survey results public...";
    }
    let options = this.alertConfig(operation);
    let alert = this.alertCtrl.create({
      title: options.title,
      subTitle: options.subTitle,
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.makeSurveyResultsPublic(loadingContent);
          }
        }
      ]
    });
    alert.present();
  }

  alertConfig(operation) {
    let options = {
      grant: { title: 'Autorizará el acceso', subTitle: 'Puede acceder a los resultados de su encuesta a través de la URL directa. ¿Estás seguro de otorgar acceso?' },
      disable: { title: 'Disable Access', subTitle: 'Your Survey results can not be accessible via direct Url. ¿Are you sure to disable access?' },

    }
    return options[operation];
  }

  mostrarGraficas(){
    this.MostrarGrafica();
  }

   //Grafica Circular
   MostrarGrafica(){
    var chart = AmCharts.makeChart( "chart_div", {
      "hideCredits": true,
      "titles": [{
        "text": "Semaforización"
      }],
      "type": "pie",
      "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
      "colors": [
        "#298A08",
        "#FFBF00",
        "#CF0B0B"
      ],
      "dataProvider": [ {
        "title": "Verde",
        "value": this.verde
      }, {
        "title": "Amarillo",
          "value": this.amarillo
        }, {
          "title": "Rojo",
          "value": this.rojo
        }],
      "titleField": "title",
      "valueField": "value",
      "labelRadius": 2,
      "fillColorsField": "color",
    
      "radius": "40%",
      "innerRadius": "0%",
      "labelText": "[[title]]"
      
    } );
    
}


  // Reset function we will use to hide the screenshot preview after 1 second
  reset() {
    var self = this;
    setTimeout(function () {
      self.state = false;

    }, 1000);
  }

  screenShot(event, fab: FabContainer) {
    fab.close();
    this.delay(1000).then(any => {
      //your task after delay.
      this.nativeStorage.getItem('storage_satisf').then(
        data => this.number_satisf = data,
        error => this.showToast(error));

      this.nativeStorage.setItem('storage_satisf', (this.number_satisf * 1) + 1);

      this.nativeStorage.getItem('storage_satisf').then(
        data => this.number_satisf = data,
        error => this.showToast(error));


      this.screenshot.save('jpg', 80, 'grafica_satisfaccion' + this.number_satisf).then(res => {
        this.screen = res.filePath;
        this.state = true;
        this.reset();
      });

      this.showToast("La imagen ha sido guardada");
    });

  }

  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: '->' + message,
      duration: 3000,
      position: 'middle'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  async delay(ms: number) {
    await new Promise(resolve => setTimeout(() => resolve(), 1000)).then(() => console.log("fired"));
  }

}
