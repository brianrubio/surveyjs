import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GraficasSatisfaccionPage } from './graficas-satisfaccion';

@NgModule({
  declarations: [
    GraficasSatisfaccionPage,
  ],
  imports: [
    IonicPageModule.forChild(GraficasSatisfaccionPage),
  ],
})
export class GraficasSatisfaccionPageModule {}
