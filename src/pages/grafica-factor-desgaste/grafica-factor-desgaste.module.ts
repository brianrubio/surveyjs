import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GraficaFactorDesgastePage } from './grafica-factor-desgaste';

@NgModule({
  declarations: [
    GraficaFactorDesgastePage,
  ],
  imports: [
    IonicPageModule.forChild(GraficaFactorDesgastePage),
  ],
})
export class GraficaFactorDesgastePageModule {}
