import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
//import { User } from '../../models/user';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
confirmar: string="";
newpass:string;
user = { email: '', password: '' };
  constructor(private afAuth: AngularFireAuth,
    public navCtrl: NavController, public navParams: NavParams,
    private alertCtrl: AlertController,public auth: AuthProvider,private toastCtrl: ToastController) {
  }

  /*async register(user:User){
        try{
          const result = await this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password);
         this.presentAlert();
        } catch(e){
          this.ErrorAlert(e);
          console.log(e);
        }

      }*/

  SignUp() {
    this.newpass = "";

    if (this.user.email == "" || this.user.password == "" || this.confirmar == "") {

      let alert = this.alertCtrl.create({
        title: 'Faltan campos por llenar',
        buttons: ['Aceptar']
      });
      alert.present();

    } else {

      if (this.confirmar == this.user.password) {
      
        this.newpass = this.user.password;
      }

      this.auth.registerUser(this.user.email, this.newpass)
        .then((user) => {
          // El usuario se ha creado correctamente
          this.presentToast();
          this.user.email = "";
          this.user.password = "";
          this.confirmar = "";
        })
        .catch(err => {
          console.log(err.message);
          if (err.message == "The email address is badly formatted.") {
            let alert = this.alertCtrl.create({
              title: 'El correo electrónico no tiene el formato correcto',
              buttons: ['Aceptar']
            });
            alert.present();
          } else if (err.message == "The password must be 6 characters long or more.") {
            if (this.user.password.length >= 6) {
            this.errorConfirmacion();
            } else {
              let alert = this.alertCtrl.create({
                title: 'La contraseña debe de tener más de 6 caracteres',
                buttons: ['Aceptar']
              });
              alert.present();
            }
          } else {
            let alert = this.alertCtrl.create({
              title: err.message,
              buttons: ['Aceptar']
            });
            alert.present();
          }
        })
    }
  }

  errorConfirmacion() {
    let alert = this.alertCtrl.create({
      title: 'Las contraseñas no coinciden',
      buttons: ['Aceptar'],
    });
    alert.present();
  }

  ErrorAlert(e) {
    let alert = this.alertCtrl.create({
      title: e,
      buttons: ['Aceptar']
    });
    alert.present();
  }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'El usuario ha sido registrado.',
      duration: 3000,
      position: 'middle'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

}
