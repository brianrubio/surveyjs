import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultadosClimaPage } from './resultados-clima';

@NgModule({
  declarations: [
    ResultadosClimaPage,
  ],
  imports: [
    IonicPageModule.forChild(ResultadosClimaPage),
  ],
})
export class ResultadosClimaPageModule {}
