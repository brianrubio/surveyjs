import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ResultadosClimaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-resultados-clima',
  templateUrl: 'resultados-clima.html',
})
export class ResultadosClimaPage {
  result: any;
  sema: number;
  FactorD:any;
  FactorD2:any;

  nombreF1:any;
  nombreF2:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.result = this.navParams.get('result');
    this.sema = this.result[5];


    this.FactorD = this.result[6];
    this.FactorD2 = this.result[8];

    if(this.FactorD=="Factor 1"){
      this.nombreF1 ="Estructura";
    } else if (this.FactorD == "Factor 2"){
      this.nombreF1 = "Integración";
    } else if (this.FactorD == "Factor 3"){
      this.nombreF1 = "Liderazgo";
    } else if (this.FactorD == "Factor 4"){
      this.nombreF1 = "Desempeño";
    } else if (this.FactorD == "Factor 5"){
      this.nombreF1 = "Satisfacción";
    }

    if(this.FactorD2=="Factor 1"){
      this.nombreF2 ="Estructura";
    } else if (this.FactorD2 == "Factor 2"){
      this.nombreF2 = "Integración";
    } else if (this.FactorD2 == "Factor 3"){
      this.nombreF2 = "Liderazgo";
    } else if (this.FactorD2 == "Factor 4"){
      this.nombreF2 = "Desempeño";
    } else if (this.FactorD2 == "Factor 5"){
      this.nombreF2 = "Satisfacción";
    }


  }

}