import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController, AlertController } from 'ionic-angular';

import { SurveyProvider } from '../../providers/survey/survey';

import { ChartsModalPage } from '../../modals/charts-modal';

import { SurveyResultsModel } from '../../models/survey.results.model';

import * as papa from 'papaparse';
import { Observable } from 'rxjs/Observable';

/**
 * Generated class for the SurveyResultsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-survey-results',
  templateUrl: 'survey-results.html',
})
export class SurveyResultsPage {

	result: Observable<any>;

	media: Observable<any>;
	
	currentYear = new Date().getFullYear();
	survey: any;
	keys: any;
	surveyResults: SurveyResultsModel[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public surveyProvider: SurveyProvider,
			  public loadingCtrl: LoadingController, public modalCtrl: ModalController, public alertCtrl: AlertController,
			  public deleteResult:SurveyProvider	
			) {

		this.survey = this.navParams.get('survey');
		this.survey.publicSurveyURL = 'https://surveyjs.io/Results/Survey/' + this.survey.Id;
		console.log("id->"+this.survey.Id)

		let loading = this.loadingCtrl.create({
            content: "Cargando resultados..."
        });

		loading.present();
		
		this.surveyProvider.getSurveyResults(this.survey.Id)
		.subscribe(
			data => {
				this.surveyResults = SurveyResultsModel.fromJSONArray(data.Data);
				console.log("id->"+this.survey.Id);
				if(this.surveyResults[0]){
					this.keys = this.surveyResults[0].userAnswers.map((val, key) => {return val['textQuestion']});
				}else{
					let Alerta = this.alertCtrl.create({
						title: "Esta encuesta no cuenta con resultados aún.",
						buttons: [
						{
							text: 'Aceptar',
							handler: data => {
							this.navCtrl.pop();
							}
						}
						]
					});
					Alerta.present();
				}
				
				//console.log("***RESULTS:" +surveyResults);

				loading.dismiss();
			},
			error => {
				console.log(<any>error);
				loading.dismiss();
			}
		);

	
  }

	openDetails(result) {
		//Desgaste ocupacional
		if (this.survey.Id == "fc724d10-65a0-4e44-bd79-38e9d931d55f"){
			
			this.navCtrl.push('ResultadosDesgastePage', { result: result, media: this.surveyResults});
		}
		//Clima laboral
		else if (this.survey.Id == "2c071c0c-1b05-4553-8bd0-318417ce3a7d"){			
		
			this.navCtrl.push('ResultadosClimaPage', { result: result, media: this.surveyResults});
		}
		//Responsabilidad Social
		else if (this.survey.Id == "32cf120a-0089-4fe3-8111-4bb17bf78e2a"){
			this.navCtrl.push('ResultadosResponsabilidadPage', { result: result, media: this.surveyResults });
		}
		//Satisfacción Laboral
		else{
			this.navCtrl.push('ResultadosSatisfaccionPage', { result: result });
		}
	}


	formatChartData() {
        const results = this.surveyResults;
        let userAnswers = [];
        // Concatenate all user's answers.
        results.map(result => { 
            userAnswers.push(...result.userAnswers)
        });

        // Group by answers by question.
        let data = userAnswers.reduce(function(rv, x) {
            (rv[x['idQuestion']] = rv[x['idQuestion']] || []).push(x.value);
            return rv;
            }, {});
        
		const chartData = Object.keys(data).map(i => data[i]);

        return chartData;
    }

	openModal() {
		let modal = this.modalCtrl.create(ChartsModalPage, {'chartData': this.formatChartData(), 'questionsText': this.keys});
		modal.present();
	}

	onClickDeleteSurveyResult() {
		let loading = this.loadingCtrl.create({
            content: "Deleting Survey..."
        });

        loading.present();

        this.surveyProvider.deleteAllSurveyResult(this.survey.Id)
        .subscribe(
            data => {
                console.log(data);
                loading.dismiss();
            },
            error => {
                console.log(<any>error);
                loading.dismiss();
            }
        );
	}

	downloadResults(result) {
		//console.log("downloadResults");********

		let csv = papa.unparse({
			fields: this.keys,
			data: this.formatChartData()
		  });
		console.log("Keys: "+this.keys);
		console.log("Datas:"+this.formatChartData());
	   
		  // Dummy implementation for Desktop download purpose.
		  // let blob = new Blob([csv]);
		  // Sent the UTF-8 header for the download process.
		  let blob = new Blob(["\ufeff", csv]);
		  let a = window.document.createElement("a");
				//create page
		  a.href = window.URL.createObjectURL(blob);

			if (this.survey.Id == "fc724d10-65a0-4e44-bd79-38e9d931d55f") {
				a.download = "desgaste-ocupacional.csv";		
				//b.download = "desgaste-ocupacional.csv";		
			}
			//Clima laboral
			else if (this.survey.Id == "2c071c0c-1b05-4553-8bd0-318417ce3a7d") {
				a.download = "clima-laboral.csv";
			//	b.download = "clima-laboral.csv";
			}
			//Responsabilidad Social
			else if (this.survey.Id == "32cf120a-0089-4fe3-8111-4bb17bf78e2a") {
				a.download = "responsabilidad-social.csv";
				//b.download = "responsabilidad-social.csv";
			}
			//Satisfacción Laboral
			else {
				a.download = "satisfaccion-laboral.csv";
				//b.download = "satisfaccion-laboral.csv";
			}
		 
		  document.body.appendChild(a);
		  a.click();
		  document.body.removeChild(a);
	}

	makeSurveyResultsPublic(content) {
		//console.log("makeSurveyResultsPublic");
		this.survey.AllowAccessResult = !this.survey.AllowAccessResult;

		let loading = this.loadingCtrl.create({
            content: content
        });
		loading.present();
		this.surveyProvider.makeSurveyResultsPublic(this.survey.Id, this.survey.AllowAccessResult)
		.subscribe(
			data => {
				// console.log(data);
				loading.dismiss();
			},
			error => {
				// console.log(<any>error);
				loading.dismiss();
			}
		);
	}

	presentAlert() {
		let operation;
		let loadingContent;
		if (this.survey.AllowAccessResult) {
			operation = "disable";
			loadingContent = "Making Survey results not public..."
		} 
		else {
			operation = "grant";
			loadingContent = "Making Survey results public...";
		} 
        let options = this.alertConfig(operation);
        let alert = this.alertCtrl.create({
          title: options.title,
          subTitle: options.subTitle,
          buttons: [
            {
                text: 'Cancelar',
                handler: () => {
                }
            },
            {
              text: 'Aceptar',
              handler: () => {
				  this.makeSurveyResultsPublic(loadingContent);
              }
            }
          ]
        });
        alert.present();
	}
	
	alertConfig(operation) {
        let options = {
            grant: {title: 'Autorizará el acceso', subTitle: 'Puede acceder a los resultados de su encuesta a través de la URL directa. ¿Estás seguro de otorgar acceso?'},
            disable: {title: 'Disable Access', subTitle: 'Your Survey results can not be accessible via direct Url. ¿Are you sure to disable access?'},

        }
        return options[operation];
	}
	
	advertencia(){
		let Alerta = this.alertCtrl.create({
		  title: "¿Esta seguro de borrar todos los resultados de las encuestas?",
		  buttons: [
			{
			  text: 'Cancelar',
			  handler: data => {
				console.log('Cancel clicked');
			  }
			},
			{
			  text: 'Aceptar',
			  handler: data => {
				this.surveyProvider.deleteAllSurveyResult(this.survey.Id);
			  }
			}
		  ]
		});
		Alerta.present();
	  
	  }

}
