import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, IonicApp } from 'ionic-angular';
import { ResultadosPage } from '../resultados/resultados';

import { RegisterPage } from '../register/register';
import { Home1aPage } from '../home1a/home1a';
import { CerrarSesionPage } from '../cerrar-sesion/cerrar-sesion';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthProvider } from '../../providers/auth/auth';
import { Survey } from 'survey-angular';
import { SurveyResultsPage } from '../survey-results/survey-results';


@Component({
  selector: 'page-tabs-admin',
  templateUrl: 'tabs-admin.html',
})
export class TabsAdminPage {


  tab1Root = Home1aPage;
  tab2Root = RegisterPage;

  

  constructor(private alertCtrl: AlertController, public auth: AuthProvider,public navCtrl: NavController, public navParams: NavParams,
   public app: IonicApp) {
  }


  qAlert() {
    let alert = this.alertCtrl.create({
      title: "¿Seguro que desea Cerrar Sesión?",
      buttons: ['Aceptar']
    });
    alert.present();
  }

  /*advertencia() {
    let Alerta = this.alertCtrl.create({
      title: "¿Está seguro que desea Cerrar Sesión?",
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
           
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Aceptar',
          handler: data => {  
          //  window.location.reload(); 
            //this.app.getRootNav().setRoot(TabsAdminPage);        
            this.auth.logout();
          }
        }
      ]
    });
    Alerta.present();

  }*/

}
