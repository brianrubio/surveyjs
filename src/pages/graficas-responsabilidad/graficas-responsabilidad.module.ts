import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GraficasResponsabilidadPage } from './graficas-responsabilidad';

@NgModule({
  declarations: [
    GraficasResponsabilidadPage,
  ],
  imports: [
    IonicPageModule.forChild(GraficasResponsabilidadPage),
  ],
})
export class GraficasResponsabilidadPageModule {}
