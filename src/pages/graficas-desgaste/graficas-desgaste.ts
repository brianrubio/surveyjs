import { Component, ErrorHandler } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController, AlertController, ToastController, FabContainer } from 'ionic-angular';

import { SurveyProvider } from '../../providers/survey/survey';
import { ChartsModalPage } from '../../modals/charts-modal';
import { SurveyResultsModel } from '../../models/survey.results.model';

import { Screenshot } from '@ionic-native/screenshot';
import { NativeStorage } from '@ionic-native/native-storage';



import * as papa from 'papaparse';
import { Observable } from 'rxjs/Observable';

declare var google;
declare var AmCharts;
declare var chart_div;
@IonicPage()
@Component({
  selector: 'page-graficas-desgaste',
  templateUrl: 'graficas-desgaste.html',
})

export class GraficasDesgastePage {
//Screen
  screen: any;
  state: boolean = false;


  number_storage:any;
	//Separator Toolbar
	resulEncuestas: string = "individual";
	  
	//Para obener resultados
	currentYear = new Date().getFullYear();
	result: Observable<any>;
	survey: any;
	keys: any;
	surveyResults: SurveyResultsModel[] = [];

	//Arreglo aux
	auxArray: any = [];

	//Objeto con resultados
	public desgasteValues: any = [];

	//Variables de Cálculos
	nuEmpleado: any;
  colorSema: any;
  
  //valores de semaforo generales
	rojo: number = 0;
	verde: number = 0;
  amarillo: number = 0;
  sumD13 = 0;
  sumD12 = 0;
  sumD11 = 0;

  //Valores de RadarChart
  SumNF1: number[] = [0, 0, 0, 0, 0, 0];
  SumNF2: number[] = [0, 0, 0, 0, 0, 0];
  SumNF3: number[] = [0, 0, 0, 0, 0, 0];
  
	p1: any; p2: any; p3: any; p4: any; p5: any; p6: any; p7: any; p8: any; p9: any; p10: any;
	p11: any; p12: any; p13: any; p14: any; p15: any; p16: any; p17: any; p18: any; p19: any; p20: any;
	p21: any; p22: any; p23: any; p24: any; p25: any; p26: any; p27: any; p28: any; p29: any; p30: any;
  
	sumF1: number;
	promedioF1: number;
	desEstF1: number;
	valZF1: number;
	valTF1: number;
	NivelF1: number;

	sumF2: number;
	promedioF2: number;
	desEstF2: number;
	valZF2: number;
	valTF2: number;
	NivelF2: number;

	sumF3: number;
	promedioF3: number;
	desEstF3: number;
	valZF3: number;
	valTF3: number;
	NivelF3: number;

	mediaf1: number;
	mediaf2: number;
	mediaf3: number;

	factorD1: any;
	factorD2: any;
	nFactorD1: any;
  nFactorD2: any;

  sumlvl1: number;
  sumlvl2: number;
  sumlvl3: number;
  sumlvl4: number;
  sumlvl5: number;
  sumlvl6: number;
  
  


	constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public surveyProvider: SurveyProvider,
    public loadingCtrl: LoadingController, 
    public modalCtrl: ModalController, 
    public alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private screenshot: Screenshot,
    private nativeStorage: NativeStorage) {

    /*nativeStorage.getItem('storage_number').then(data => {
      if (data) {
        this.number = data;
      }
      else {
        this.number = 0;
      }
    }); (error) => {
      alert(error);
    }*/
    


		this.survey = this.navParams.get('survey');
		this.survey.publicSurveyURL = 'https://surveyjs.io/Results/Survey/' + this.survey.Id;
		//console.log("id->" + this.survey.Id)

		let loading = this.loadingCtrl.create({
		content: "Cargando resultados..."
		});

		loading.present();

		this.surveyProvider.getSurveyResults(this.survey.Id)
		.subscribe(
			data => {
        this.surveyResults = SurveyResultsModel.fromJSONArray(data.Data);
        if(this.surveyResults[0]){
          this.keys = this.surveyResults[0].userAnswers.map((val, key) => { return val['textQuestion'] });
        }else{
          let Alerta = this.alertCtrl.create({
            title: "Esta encuesta aún no cuenta con registros.",
            buttons: [
            {
              text: 'Aceptar',
              handler: data => {
                this.navCtrl.pop();
              }
            }
            ]
          });
          Alerta.present();
        }

       nativeStorage.getItem('storage_number').then(
          data => this.number_storage= data,
          error => this.nativeStorage.setItem('storage_number', 0)
        );
       // this.showToast(this.number_storage);
				
				//DATAS HERE
				// console.log("***RESULTS:",this.surveyResults);
        this.sumlvl1 = 0;
        this.sumlvl2 = 0;
        this.sumlvl3 = 0;
        this.sumlvl4 = 0;
        this.sumlvl5 = 0;
        this.sumlvl6 = 0;

				for (let a = 0; a <= this.surveyResults.length -1; a++) {
					this.nuEmpleado=(this.surveyResults[a].userAnswers[0].value);

					this.mediaf1 = 0; 
					this.mediaf2 = 0; 
					this.mediaf3 = 0; 

					var i;
					var j;
					for (i in this.surveyResults) {
						for (j in this.surveyResults[i].userAnswers) {
							//Factor 1
							if (j == "6" || j == "8" || j == 9 || j == 10 || j == 18 || j == 23 || j == 24 || j == 30 || j == 31) {
								this.mediaf1 = (parseInt(this.surveyResults[i].userAnswers[j].value) * 1) + this.mediaf1;
							}
							//Factor 2 
							if (j == 7 || j == 11 || j == 13 || j == 20 || j == 22 || j == 25 || j == 26 || j == 27 || j == 33) {
								this.mediaf2 = (parseInt(this.surveyResults[i].userAnswers[j].value) * 1) + this.mediaf2;
							}
							//Factor 3
							if (j == 5 || j == 12 || j == 14 || j == 15 || j == 16 || j == 17 || j == 27 || j == 19 || j == 21
								|| j == 28 || j == 29 || j == 32 || j == 34) {
								this.mediaf3 = (parseInt(this.surveyResults[i].userAnswers[j].value) * 1) + this.mediaf3;
							}
							if (j >= 29 && j <= 32) {
								this.mediaf3 = (parseInt(this.surveyResults[i].userAnswers[j].value) * 1) + this.mediaf3;
							}
						}
					}

					this.mediaf1 = this.mediaf1 / ((i * 1 + 1) * 9);


					this.mediaf2 = this.mediaf2 / ((i * 1 + 1) * 9);


					this.mediaf3 = this.mediaf3 / ((i * 1 + 1) * 12);
				
					this.p1 = this.surveyResults[a].userAnswers[5].value;
					this.p2 = this.surveyResults[a].userAnswers[6].value;
					this.p3 = this.surveyResults[a].userAnswers[7].value;
					this.p4 = this.surveyResults[a].userAnswers[8].value;
					this.p5 = this.surveyResults[a].userAnswers[9].value;
					this.p6 = this.surveyResults[a].userAnswers[10].value;
					this.p7 = this.surveyResults[a].userAnswers[11].value;
					this.p8 = this.surveyResults[a].userAnswers[12].value;
					this.p9 = this.surveyResults[a].userAnswers[13].value;

					this.p10 = this.surveyResults[a].userAnswers[14].value;
					this.p11 = this.surveyResults[a].userAnswers[15].value;
					this.p12 = this.surveyResults[a].userAnswers[16].value;
					this.p13 = this.surveyResults[a].userAnswers[17].value;

					this.p14 = this.surveyResults[a].userAnswers[18].value;
					this.p15 = this.surveyResults[a].userAnswers[19].value;
					this.p16 = this.surveyResults[a].userAnswers[20].value;
					this.p17 = this.surveyResults[a].userAnswers[21].value;
					this.p18 = this.surveyResults[a].userAnswers[22].value;
					this.p19 = this.surveyResults[a].userAnswers[23].value;
					this.p20 = this.surveyResults[a].userAnswers[24].value;

					this.p21 = this.surveyResults[a].userAnswers[25].value;
					this.p22 = this.surveyResults[a].userAnswers[26].value;
					this.p23 = this.surveyResults[a].userAnswers[27].value;
					this.p24 = this.surveyResults[a].userAnswers[28].value;
					this.p25 = this.surveyResults[a].userAnswers[29].value;
					this.p26 = this.surveyResults[a].userAnswers[30].value;
					this.p27 = this.surveyResults[a].userAnswers[31].value;
					this.p28 = this.surveyResults[a].userAnswers[32].value;
					this.p29 = this.surveyResults[a].userAnswers[33].value;
					this.p30 = this.surveyResults[a].userAnswers[34].value;

					this.CalcularSemaforo();

					this.CalcularSumF1();
					this.CalcularPromedioF1();
					this.CalcularDesEstF1();
					this.CalcularValZF1();
					this.CalcularTF1();
					this.CalcularNivelF1();

					this.CalcularSumF2();
					this.CalcularPromedioF2();
					this.CalcularDesEstF2() ;
					this.CalcularValZF2();
					this.CalcularTF2();
					this.CalcularNivelF2();

					this.CalcularSumF3();
					this.CalcularPromedioF3();
					this.CalcularDesEstF3();
					this.CalcularValZF3();
					this.CalcularTF3();
          this.CalcularNivelF3();
          //this.MostrarGrafica();
          //this.MostrarGraficaBarras();

					var D1 = Math.min(this.NivelF1, this.NivelF2, this.NivelF3);

					this.factorD1 = this.NivelF1 == D1 ? "Factor 1" :
					this.NivelF2 == D1 ? "Factor 2" :
					this.NivelF3 == D1 ? "Factor 3" : null;

					var D2;

					if (this.factorD1 == "Factor 1") {
            //sumatoria
            this.sumD11 += 1;

						this.nFactorD1 = this.NivelF1;
						D2 = Math.min(this.NivelF2, this.NivelF3);

						this.factorD2 =
						this.NivelF2 == D2 ? "Factor 2" :
						this.NivelF3 == D2 ? "Factor 3" : null;

					}
					
					if (this.factorD1 == "Factor 2") {
            //sumatoria
            this.sumD12 += 1;
						this.nFactorD1 = this.NivelF2;
						D2 = Math.min(this.NivelF1, this.NivelF3);

						this.factorD2 =
						this.NivelF1 == D2 ? "Factor 1" :
						this.NivelF3 == D2 ? "Factor 3" : null;
					}
					
					if (this.factorD1 == "Factor 3") {
            //sumatoria
            this.sumD13 += 1;
						this.nFactorD1 = this.NivelF3;
						D2 = Math.min(this.NivelF1, this.NivelF2);

						this.factorD2 =
						this.NivelF1 == D2 ? "Factor 1" :
						this.NivelF2 == D2 ? "Factor 2" : null;
          }
        

					if (this.factorD2 == "Factor 1") {
					this.nFactorD2 = this.NivelF1;
					}
					if (this.factorD2 == "Factor 2") {
					this.nFactorD2 = this.NivelF2;
					}
					if (this.factorD2 == "Factor 3") {
					this.nFactorD2 = this.NivelF3;
					}

          //contadores del semaforo
					if (this.colorSema >= 0 && this.colorSema < 2.5 ){
					this.rojo =(this.rojo *1 )+ 1; 
					} else if (this.colorSema >= 2.5 && this.colorSema < 3.3 ){
					this.amarillo = (this.amarillo * 1) + 1;
					} else if (this.colorSema >= 3.3 && this.colorSema <= 5 ){
					this.verde = (this.verde * 1) + 1;             
          }
          
          //nfactor sumatoria
          console.log("Factoooor:"+this.nFactorD1);
          
         
          switch (this.nFactorD1) {
            case 1: this.sumlvl1 = (this.sumlvl1 * 1) + 1; break;
            case 2: this.sumlvl2 = (this.sumlvl2 * 1) + 1; break;
            case 3: this.sumlvl3 = (this.sumlvl3 * 1) + 1; break;
            case 4: this.sumlvl4 = (this.sumlvl4 * 1) + 1; break;
            case 5: this.sumlvl5 = (this.sumlvl5 * 1) + 1; break;
            case 6: this.sumlvl6 = (this.sumlvl6 * 1) + 1; break;

          }
 
          //Sumatoria de niveles (RadarChart)
          switch (this.NivelF1) {
            case 1: this.SumNF1[0] = (this.SumNF1[0] * 1) + 1; break;
            case 2: this.SumNF1[1] = (this.SumNF1[1] * 1) + 1; break;
            case 3: this.SumNF1[2] = (this.SumNF1[2] * 1) + 1; break;
            case 4: this.SumNF1[3] = (this.SumNF1[3] * 1) + 1; break;
            case 5: this.SumNF1[4] = (this.SumNF1[4] * 1) + 1; break;
            case 6: this.SumNF1[5] = (this.SumNF1[5] * 1) + 1; break;

          }

          switch (this.NivelF2) {
            case 1: this.SumNF2[0] = (this.SumNF2[0] * 1) + 1; break;
            case 2: this.SumNF2[1] = (this.SumNF2[1] * 1) + 1; break;
            case 3: this.SumNF2[2] = (this.SumNF2[2] * 1) + 1; break;
            case 4: this.SumNF2[3] = (this.SumNF2[3] * 1) + 1; break;
            case 5: this.SumNF2[4] = (this.SumNF2[4] * 1) + 1; break;
            case 6: this.SumNF2[5] = (this.SumNF2[5] * 1) + 1; break;

          }

          switch (this.NivelF3) {
            case 1: this.SumNF3[0] = (this.SumNF3[0] * 1) + 1; break;
            case 2: this.SumNF3[1] = (this.SumNF3[1] * 1) + 1; break;
            case 3: this.SumNF3[2] = (this.SumNF3[2] * 1) + 1; break;
            case 4: this.SumNF3[3] = (this.SumNF3[3] * 1) + 1; break;
            case 5: this.SumNF3[4] = (this.SumNF3[4] * 1) + 1; break;
            case 6: this.SumNF3[5] = (this.SumNF3[5] * 1) + 1; break;

          }

          
          

					//Limpiar Arreglo Auxiliar
					this.auxArray= [];

					//Factores en consola
					console.log(this.factorD1);
					console.log(this.nFactorD1);
				
					console.log(this.factorD2);
					console.log(this.nFactorD2);

					//Crear Arreglo

						//Datos personales
					this.auxArray.push(this.nuEmpleado);
							//Edad
					this.auxArray.push(this.surveyResults[a].userAnswers[1].value);
							//Estado Civil
					this.auxArray.push(this.surveyResults[a].userAnswers[2].value);
							//Género
					this.auxArray.push(this.surveyResults[a].userAnswers[3].value);
							//Antiguedad
					this.auxArray.push(this.surveyResults[a].userAnswers[4].value);

						//Semáforo
					this.auxArray.push(this.colorSema);
						//Factores Débiles
					this.auxArray.push(this.factorD1);
					this.auxArray.push(this.nFactorD1);
					this.auxArray.push(this.factorD2);
					this.auxArray.push(this.nFactorD2);

					//Create Object
					this.desgasteValues.push(this.auxArray)
			
				}
			
			console.log(this.auxArray);
			console.log(this.desgasteValues);
        console.log("f1"+this.sumD11);
        console.log("F2"+this.sumD12);
        console.log("F3"+this.sumD13);
        console.log("Suma1" + this.sumlvl1);
        console.log("Suma1" + this.sumlvl2);
        console.log("Suma1" + this.sumlvl3);
        console.log("Suma1" + this.sumlvl4);
        console.log("Suma1" + this.sumlvl5);


			
			loading.dismiss();
    	},
        error => {
			console.log(<any>error);
			loading.dismiss();
        }
	);
	
	this.result=this.desgasteValues;

  }

  openDetails(result) {
   
	  this.navCtrl.push('ResultadosDesgastePage', { result: result});
   
  }


  formatChartData() {
    const results = this.surveyResults;
    let userAnswers = [];
    // Concatenate all user's answers.
    results.map(result => {
      userAnswers.push(...result.userAnswers)
    });

    // Group by answers by question.
    let data = userAnswers.reduce(function (rv, x) {
      (rv[x['idQuestion']] = rv[x['idQuestion']] || []).push(x.value);
      return rv;
    }, {});

    const chartData = Object.keys(data).map(i => data[i]);

    return chartData;
  }

  openModal() {
    let modal = this.modalCtrl.create(ChartsModalPage, { 'chartData': this.formatChartData(), 'questionsText': this.keys });
    modal.present();
  }

  downloadResults(result) {
    //console.log("downloadResults");********

    let csv = papa.unparse({
      fields: this.keys,
      data: this.formatChartData()
    });
   // console.log(this.keys);
    //console.log(this.formatChartData());

    // Dummy implementation for Desktop download purpose.
    // let blob = new Blob([csv]);
    // Sent the UTF-8 header for the download process.
    let blob = new Blob(["\ufeff", csv]);
    let a = window.document.createElement("a");
    //create page
    a.href = window.URL.createObjectURL(blob);

   
      a.download = "desgaste-ocupacional.csv";	
   

    //document.body.appendChild(a);
    a.click();
    //document.body.removeChild(a);
  }

  makeSurveyResultsPublic(content) {
    //console.log("makeSurveyResultsPublic");
    this.survey.AllowAccessResult = !this.survey.AllowAccessResult;

    let loading = this.loadingCtrl.create({
      content: content
    });
    loading.present();
    this.surveyProvider.makeSurveyResultsPublic(this.survey.Id, this.survey.AllowAccessResult)
      .subscribe(
        data => {
          // console.log(data);
          loading.dismiss();
        },
        error => {
          // console.log(<any>error);
          loading.dismiss();
        }
      );
  }

  presentAlert() {
    let operation;
    let loadingContent;
    if (this.survey.AllowAccessResult) {
      operation = "disable";
      loadingContent = "Making Survey results not public..."
    }
    else {
      operation = "grant";
      loadingContent = "Making Survey results public...";
    }
    let options = this.alertConfig(operation);
    let alert = this.alertCtrl.create({
      title: options.title,
      subTitle: options.subTitle,
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.makeSurveyResultsPublic(loadingContent);
          }
        }
      ]
    });
    alert.present();
  }

  alertConfig(operation) {
    let options = {
      grant: { title: 'Autorizará el acceso', subTitle: 'Puede acceder a los resultados de su encuesta a través de la URL directa. ¿Estás seguro de otorgar acceso?' },
      disable: { title: 'Disable Access', subTitle: 'Your Survey results can not be accessible via direct Url. ¿Are you sure to disable access?' },

    }
    return options[operation];
  }




  //Funciones de cálculo
  CalcularSemaforo() {
    this.colorSema = (this.p1 * 1 + this.p2 * 1 + this.p3 * 1 + this.p4 * 1 + this.p5 * 1 +
      this.p6 * 1 + this.p7 * 1 + this.p8 * 1 + this.p9 * 1 + this.p10 * 1 +
      this.p11 * 1 + this.p12 * 1 + this.p13 * 1 + this.p14 * 1 + this.p15 * 1 +
      this.p16 * 1 + this.p17 * 1 + this.p18 * 1 + this.p19 * 1 + this.p20 * 1 +
      this.p21 * 1 + this.p22 * 1 + this.p23 * 1 + this.p24 * 1 + this.p25 * 1 +
      this.p26 * 1 + this.p27 * 1 + this.p28 * 1 + this.p29 * 1 + this.p30 * 1) / 30;
  }


  //Formulas del Factor 01
  CalcularSumF1() {
    this.sumF1 = (this.p2 * 1 + this.p4 * 1 + this.p5 * 1 + this.p6 * 1 + this.p14 * 1 +
      this.p19 * 1 + this.p20 * 1 + this.p26 * 1 + this.p27 * 1);
    if (this.sumF1 == 45) {
      this.sumF1 = this.sumF1 * 1 - 1;
      this.p27 = 4;

    }

  }

  CalcularPromedioF1() {
    this.promedioF1 = (this.sumF1 / 9);

  }

  CalcularDesEstF1() {
    this.desEstF1 = Math.sqrt(
      ((Math.pow(Math.abs(this.p2 * 1 - this.promedioF1 * 1), 2)) +
        (Math.pow(Math.abs(this.p4 * 1 - this.promedioF1 * 1), 2)) +
        (Math.pow(Math.abs(this.p5 * 1 - this.promedioF1 * 1), 2)) +
        (Math.pow(Math.abs(this.p6 * 1 - this.promedioF1 * 1), 2)) +
        (Math.pow(Math.abs(this.p14 * 1 - this.promedioF1 * 1), 2)) +
        (Math.pow(Math.abs(this.p19 * 1 - this.promedioF1 * 1), 2)) +
        (Math.pow(Math.abs(this.p20 * 1 - this.promedioF1 * 1), 2)) +
        (Math.pow(Math.abs(this.p26 * 1 - this.promedioF1 * 1), 2)) +
        (Math.pow(Math.abs(this.p27 * 1 - this.promedioF1 * 1), 2))) / 8);

  }

  CalcularValZF1() {
    this.valZF1 = (this.promedioF1 - this.mediaf1) / this.desEstF1;
  }

  CalcularTF1() {
    this.valTF1 = Math.round(50 + (this.valZF1 * 10));
  }

  CalcularNivelF1() {
    if (this.valTF1 < 30) {
      this.NivelF1 = 1;
    } else if (this.valTF1 >= 30 && this.valTF1 < 40) {
      this.NivelF1 = 2;
    } else if (this.valTF1 >= 40 && this.valTF1 < 51) {
      this.NivelF1 = 3;
    } else if (this.valTF1 >= 51 && this.valTF1 < 61) {
      this.NivelF1 = 4;
    } else if (this.valTF1 >= 61 && this.valTF1 < 71) {
      this.NivelF1 = 5;
    } else if (this.valTF1 >= 71) {
      this.NivelF1 = 6;
    }
  }

  //Fórmulas del Factor 02
  CalcularSumF2() {
    this.sumF2 = (this.p3 * 1 + this.p7 * 1 + this.p9 * 1 + this.p16 * 1 + this.p18 * 1 +
      this.p21 * 1 + this.p22 * 1 + this.p23 * 1 + this.p29 * 1);
    if (this.sumF2 == 45) {
      this.sumF2 = this.sumF2 * 1 - 1;
      this.p29 = 4;
    }
  }

  CalcularPromedioF2() {
    this.promedioF2 = (this.sumF2 / 9);
  }

  CalcularDesEstF2() {
    this.desEstF2 = Math.sqrt(
      ((Math.pow(Math.abs(this.p3 * 1 - this.promedioF1 * 1), 2)) +
        (Math.pow(Math.abs(this.p7 * 1 - this.promedioF1 * 1), 2)) +
        (Math.pow(Math.abs(this.p9 * 1 - this.promedioF1 * 1), 2)) +
        (Math.pow(Math.abs(this.p16 * 1 - this.promedioF1 * 1), 2)) +
        (Math.pow(Math.abs(this.p18 * 1 - this.promedioF1 * 1), 2)) +
        (Math.pow(Math.abs(this.p21 * 1 - this.promedioF1 * 1), 2)) +
        (Math.pow(Math.abs(this.p22 * 1 - this.promedioF1 * 1), 2)) +
        (Math.pow(Math.abs(this.p23 * 1 - this.promedioF1 * 1), 2)) +
        (Math.pow(Math.abs(this.p29 * 1 - this.promedioF1 * 1), 2))) / 8);
  }

  CalcularValZF2() {
    this.valZF2 = (this.promedioF2 - this.mediaf2) / this.desEstF2;
  }

  CalcularTF2() {
    this.valTF2 = Math.round(50 + (this.valZF2 * 10));
  }

  CalcularNivelF2() {
    if (this.valTF2 < 30) {
      this.NivelF2 = 1;
    } else if (this.valTF2 >= 30 && this.valTF2 < 40) {
      this.NivelF2 = 2;
    } else if (this.valTF2 >= 40 && this.valTF2 < 51) {
      this.NivelF2 = 3;
    } else if (this.valTF2 >= 51 && this.valTF2 < 61) {
      this.NivelF2 = 4;
    } else if (this.valTF2 >= 61 && this.valTF2 < 71) {
      this.NivelF2 = 5;
    } else if (this.valTF2 >= 71) {
      this.NivelF2 = 6;
    }
  }

  //Fórmulas del Factor 03
  CalcularSumF3() {
    this.sumF3 = (this.p1 * 1 + this.p8 * 1 + this.p10 * 1 + this.p11 * 1 + this.p12 * 1 +
      this.p13 * 1 + this.p15 * 1 + this.p17 * 1 + this.p24 * 1 + this.p25 * 1 +
      this.p28 * 1 + this.p30 * 1);
    if (this.sumF3 == 60) {
      this.sumF3 = this.sumF3 * 1 - 1;
      this.p30 = 4;
    }
  }

  CalcularPromedioF3() {
    this.promedioF3 = (this.sumF3 / 12);
  }

  CalcularDesEstF3() {
    this.desEstF3 = Math.sqrt(
      ((Math.pow(Math.abs(this.p1 - this.promedioF3), 2)) +
        (Math.pow(Math.abs(this.p8 - this.promedioF3), 2)) +
        (Math.pow(Math.abs(this.p10 - this.promedioF3), 2)) +
        (Math.pow(Math.abs(this.p11 - this.promedioF3), 2)) +
        (Math.pow(Math.abs(this.p12 - this.promedioF3), 2)) +
        (Math.pow(Math.abs(this.p13 - this.promedioF3), 2)) +
        (Math.pow(Math.abs(this.p15 - this.promedioF3), 2)) +
        (Math.pow(Math.abs(this.p17 - this.promedioF3), 2)) +
        (Math.pow(Math.abs(this.p24 - this.promedioF3), 2)) +
        (Math.pow(Math.abs(this.p25 - this.promedioF3), 2)) +
        (Math.pow(Math.abs(this.p28 - this.promedioF3), 2)) +
        (Math.pow(Math.abs(this.p30 - this.promedioF3), 2))) / 11);
  }

  CalcularValZF3() {
    this.valZF3 = (this.promedioF3 - this.mediaf3) / this.desEstF3;
  }

  CalcularTF3() {
    this.valTF3 = Math.round(50 + (this.valZF3 * 10));
  }

  CalcularNivelF3() {
    if (this.valTF3 < 30) {
      this.NivelF3 = 1;
    } else if (this.valTF3 >= 30 && this.valTF3 < 40) {
      this.NivelF3 = 2;
    } else if (this.valTF3 >= 40 && this.valTF3 < 51) {
      this.NivelF3 = 3;
    } else if (this.valTF3 >= 51 && this.valTF3 < 61) {
      this.NivelF3 = 4;
    } else if (this.valTF3 >= 61 && this.valTF3 < 71) {
      this.NivelF3 = 5;
    } else if (this.valTF3 >= 71) {
      this.NivelF3 = 6;
    }
  }
  //Grafica Circular
  MostrarGrafica(){
        var chart = AmCharts.makeChart( "chart_div", {
          "hideCredits": true,
          "titles": [{
            "text": "Semaforización"
          }],
          "type": "pie",
          "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
          "colors": [
            "#298A08",
            "#FFBF00",
            "#CF0B0B"
          ],
          "dataProvider": [ {
            "title": "Verde",
            "value": this.verde
          }, {
            "title": "Amarillo",
              "value": this.amarillo
            }, {
              "title": "Rojo",
              "value": this.rojo
            }],
          "titleField": "title",
          "valueField": "value",
          "labelRadius": 2,
          "fillColorsField": "color",
        
          "radius": "40%",
          "innerRadius": "0%",
          "labelText": "[[title]]"
          
        } );
        
  }
  //Grafica de barras
  MostrarGraficaBarras(){
    var chart = AmCharts.makeChart("chart_div2", {
      "hideCredits": true, 
      "titles": [{
        "text": "Factor más débil"
      }],
          "theme": "light",
          "type": "serial",
        "startDuration": 0,
          "dataProvider": [{
              "factor": "Agotamiento",
            "value": this.sumD11,
            "color": "#F44336"
          }, {
              "factor": "Despersonalización",
              "value": this.sumD12,
              "color": "#FB8C00"
            }, {
              "factor": "Insatisfacción de logro",
              "value": this.sumD13,
              "color": "#FFEE58"
            }],
          "valueAxes": [{
              "position": "left",
              "title": "Número de personas"
          }],
          "graphs": [{
              "balloonText": "[[category]]: <b>[[value]]</b>",
              "fillColorsField": "color",
              "fillAlphas": 1,
              "lineAlpha": 0.1,
              "type": "column",
              "valueField": "value"
          }],
          "depth3D": 20,
        "angle": 30,
          "chartCursor": {
              "categoryBalloonEnabled": false,
              "cursorAlpha": 0,
              "zoomable": false
          },
          "categoryField": "factor",
          "categoryAxis": {
              "gridPosition": "start",
              "labelRotation": 90
          }
    
      
      });

  }

  GraficarNiveles(){
    var chart = AmCharts.makeChart("chart_div3", {
      "hideCredits": true,
      "titles": [{
        "text": "Nivel más débil"
      }],
      "theme": "light",
      "type": "serial",
      "startDuration": 0,
      "dataProvider": [{
        "nivel": "Nivel 1",
        "value": this.sumlvl1,
        "color": "#922B21"
      }, {
          "nivel": "Nivel 2",
          "value": this.sumlvl2,
          "color": "#F44336"
      }, {
          "nivel": "Nivel 3",
          "value": this.sumlvl3,
          "color": "#FB8C00"
        }, {
          "nivel": "Nivel 4",
          "value": this.sumlvl4,
          "color": "#FFEE58"
        }, {
          "nivel": "Nivel 5",
          "value": this.sumlvl5,
          "color": "#81C784"
        }, {
          "nivel": "Nivel 6",
          "value": this.sumlvl6,
          "color": "#03A9F4"
        }],
      "valueAxes": [{
        "position": "left",
        "title": "Número de personas"
      }],
      "graphs": [{
        "balloonText": "[[category]]: <b>[[value]]</b>",
        "fillColorsField": "color",
        "fillAlphas": 1,
        "lineAlpha": 0.1,
        "type": "column",
        "valueField": "value"
      }],
      "depth3D": 20,
      "angle": 30,
      "chartCursor": {
        "categoryBalloonEnabled": false,
        "cursorAlpha": 0,
        "zoomable": false
      },
      "categoryField": "nivel",
      "categoryAxis": {
        "gridPosition": "start",
        "labelRotation": 90
      }
     

    });
  }

  graficaRadarF1(){
    //chart = new google.visualization.PieChart(document.getElementById('chartdiv'));
    //document.getElementById('chartdiv')
    var chart = AmCharts.makeChart( "chartdiv", {
      "hideCredits": true,
      "titles": [{
        "text": "Agotamiento"
      }],
      "type": "radar",
      "theme": "light",
      "dataProvider": [ {
        "categoria": "Nvl. 1",
        "empleados": this.SumNF1[0]
      }, {
        "categoria": "Nvl. 2",
        "empleados": this.SumNF1[1]
      }, {
        "categoria": "Nvl. 3",
        "empleados": this.SumNF1[2]
      }, {
        "categoria": "Nvl. 4",
        "empleados": this.SumNF1[3]
      }, {
        "categoria": "Nvl. 5",
        "empleados": this.SumNF1[4]
      }, {
        "categoria": "Nvl. 6",
        "empleados": this.SumNF1[5]
      } ],
      "valueAxes": [ {
        "axisTitleOffset": 20,
        "minimum": 0,
        "axisAlpha": 0.15
      } ],
      "startDuration": 0,
      "graphs": [ {
        "balloonText": "[[value]] empleados",
        "bullet": "round",
        "lineThickness": 2,
        "valueField": "empleados"
      } ],
      "categoryField": "categoria"
     
     
    } );
  }

  graficaRadarF2(){
    //chart = new google.visualization.PieChart(document.getElementById('chartdiv2'));
    //document.getElementById('chartdiv2')
    var chart = AmCharts.makeChart( "chartdiv2", {
      "hideCredits": true,
      "titles": [{
        "text": "Despersonalización"
      }],
      "type": "radar",
      "theme": "light",
      "dataProvider": [ {
        "categoria": "Nvl. 1",
        "empleados": this.SumNF2[0]
      }, {
        "categoria": "Nvl. 2",
        "empleados": this.SumNF2[1]
      }, {
        "categoria": "Nvl. 3",
        "empleados": this.SumNF2[2]
      }, {
        "categoria": "Nvl. 4",
        "empleados": this.SumNF2[3]
      }, {
        "categoria": "Nvl. 5",
        "empleados": this.SumNF2[4]
      }, {
        "categoria": "Nvl. 6",
        "empleados": this.SumNF2[5]
      } ],
      "valueAxes": [ {
        "axisTitleOffset": 20,
        "minimum": 0,
        "axisAlpha": 0.15
      } ],
      "startDuration": 0,
      "graphs": [ {
        "balloonText": "[[value]] empleados",
        "bullet": "round",
        "lineThickness": 2,
        "valueField": "empleados"
      } ],
      "categoryField": "categoria"
     
     
    } );
  }

  graficaRadarF3(){
    //chart = new google.visualization.PieChart(document.getElementById('chartdiv3'));
    //document.getElementById('chartdiv3')
    var chart = AmCharts.makeChart( "chartdiv3", {
      "hideCredits": true,
      "titles": [{
        "text": "Insatisfacción de logro"
      }],
      "type": "radar",
      "theme": "light",
      "dataProvider": [ {
        "categoria": "Nvl. 1",
        "empleados": this.SumNF3[0]
      }, {
        "categoria": "Nvl. 2",
        "empleados": this.SumNF3[1]
      }, {
        "categoria": "Nvl. 3",
        "empleados": this.SumNF3[2]
      }, {
        "categoria": "Nvl. 4",
        "empleados": this.SumNF3[3]
      }, {
        "categoria": "Nvl. 5",
        "empleados": this.SumNF3[4]
      }, {
        "categoria": "Nvl. 6",
        "empleados": this.SumNF3[5]
      } ],
      "valueAxes": [ {
        "axisTitleOffset": 20,
        "minimum": 0,
        "axisAlpha": 0.15
      } ],
      "startDuration": 0,
      "graphs": [ {
        "balloonText": "[[value]] empleados",
        "bullet": "round",
        "lineThickness": 2,
        "valueField": "empleados"
      } ],
      "categoryField": "categoria"
    
     
    } );
  }

  warninigDeleteAllResults(event, fab: FabContainer){  
    fab.close(); 
    let Alerta = this.alertCtrl.create({
      title: "¿Está seguro de borrar todos los registros de la encuesta Desgaste Ocupacional?",
      buttons: [
      {
        text: 'Cancelar',
        handler: data => {
        console.log('Cancel clicked');
        }
      },
      {
        text: 'Aceptar',
        handler: data => {
        this.onClickDeleteSurveyResult();
        }
      }
      ]
    });
    Alerta.present();
  }
  
      onClickDeleteSurveyResult() {
        let loading = this.loadingCtrl.create({
                content: "Eliminando las respuestas de todas las encuestas."
            });
    
            loading.present().then(()=>{
                    this.surveyProvider.deleteAllSurveyResult(this.survey.Id)
                        .subscribe(
                            data => {
                            console.log(data);
                        },
                        error => {
                        console.log(<any>error);
                        }
                    );
                loading.dismiss();   
            }
          );  
          this.navCtrl.pop(); 
        this.presentToast();
      }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Los registros de la encuesta Desgaste Ocupacional han sido eliminadoss',
      duration: 3000,
      position: 'middle'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }


  mostrarGraficas(){
    this.GraficarNiveles();
    this.MostrarGrafica();
    this.MostrarGraficaBarras();

    this.graficaRadarF1();
    this.graficaRadarF2();
    this.graficaRadarF3();
  
  }

  // Reset function we will use to hide the screenshot preview after 1 second
  reset() {
    var self = this;
    setTimeout(function () {
      self.state = false;
      
    }, 1000);
  }

  screenShot(event, fab: FabContainer) {
    fab.close();
    this.delay(1000).then(any => {
      //your task after delay.
      this.nativeStorage.getItem('storage_number').then(
         data => this.number_storage = data,
         error => this.showToast(error));
   
       this.nativeStorage.setItem('storage_number', (this.number_storage*1)+1 );
   
       this.nativeStorage.getItem('storage_number').then(
         data => this.number_storage = data,
         error => this.showToast(error));
   
   
       this.screenshot.save('jpg', 80, 'grafica_desgaste' + this.number_storage).then(res => {
         this.screen = res.filePath;
         this.state = true;
          this.reset();
       });
   
       this.showToast("La imagen ha sido guardada");
    });

  }


  showToast(message:string) {
    let toast = this.toastCtrl.create({
      message: '->' + message,
      duration: 3000,
      position: 'middle'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  async delay(ms: number) {
  await new Promise(resolve => setTimeout(() => resolve(), 1000)).then(() => console.log("fired"));
}
  
}
