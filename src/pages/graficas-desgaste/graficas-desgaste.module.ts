import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GraficasDesgastePage } from './graficas-desgaste';

@NgModule({
  declarations: [
    GraficasDesgastePage,
  ],
  imports: [
    IonicPageModule.forChild(GraficasDesgastePage),
  ],
})
export class GraficasDesgastePageModule {}
