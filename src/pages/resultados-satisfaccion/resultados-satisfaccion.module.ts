import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultadosSatisfaccionPage } from './resultados-satisfaccion';

@NgModule({
  declarations: [
    ResultadosSatisfaccionPage,
  ],
  imports: [
    IonicPageModule.forChild(ResultadosSatisfaccionPage),
  ],
})
export class ResultadosSatisfaccionPageModule {}
