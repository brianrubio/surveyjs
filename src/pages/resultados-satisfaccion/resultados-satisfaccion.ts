import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ResultadosSatisfaccionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-resultados-satisfaccion',
  templateUrl: 'resultados-satisfaccion.html',
})
export class ResultadosSatisfaccionPage {
  result: any;
  sema: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.result = this.navParams.get('result');
    this.sema = this.result[5];

  }
}