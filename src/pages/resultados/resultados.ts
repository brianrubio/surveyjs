import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SurveyResultsPage } from '../survey-results/survey-results';

/**
 * Generated class for the ResultadosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-resultados',
  templateUrl: 'resultados.html',
})
export class ResultadosPage {
  currentYear = new Date().getFullYear();
  survey: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.survey = this.navParams.get('survey');
  }

  getSurveyResults() {
    this.navCtrl.push(SurveyResultsPage, {
      survey: this.survey
    });
  }
  

}
