import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultadosResponsabilidadPage } from './resultados-responsabilidad';

@NgModule({
  declarations: [
    ResultadosResponsabilidadPage,
  ],
  imports: [
    IonicPageModule.forChild(ResultadosResponsabilidadPage),
  ],
})
export class ResultadosResponsabilidadPageModule {}
