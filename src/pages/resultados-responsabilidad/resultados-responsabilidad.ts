import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ResultadosResponsabilidadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-resultados-responsabilidad',
  templateUrl: 'resultados-responsabilidad.html',
})
export class ResultadosResponsabilidadPage {
  //Variables locales para los resultados
  result: any;
  sema: number;

  FactorD: any;
  FactorD2: any;

  nombreF1: any;
  nombreF2: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    //Obtiene los resultados de las encuestas
    this.result = this.navParams.get('result');
    this.sema = this.result[5];



    this.FactorD = this.result[6];
    this.FactorD2 = this.result[8];

    if (this.FactorD == "Factor 1") {
      this.nombreF1 = "Gobernanza";
    } else if (this.FactorD == "Factor 2") {
      this.nombreF1 = "Derechos Humanos";
    } else if (this.FactorD == "Factor 3") {
      this.nombreF1 = "Prácticas Laborales";
    } else if (this.FactorD == "Factor 4") {
      this.nombreF1 = "Medio Ambiente";
    } else if (this.FactorD == "Factor 5") {
      this.nombreF1 = "Prácticas Justas";
    } else if (this.FactorD == "Factor 6") {
      this.nombreF1 = "Consumidores";
    } else if (this.FactorD == "Factor 7") {
      this.nombreF1 = "Participación Activa";
    }

    if (this.FactorD2 == "Factor 1") {
      this.nombreF2 = "Gobernanza";
    } else if (this.FactorD2 == "Factor 2") {
      this.nombreF2 = "Derechos Humanos";
    } else if (this.FactorD2 == "Factor 3") {
      this.nombreF2 = "Prácticas Laborales";
    } else if (this.FactorD2 == "Factor 4") {
      this.nombreF2 = "Medio Ambiente";
    } else if (this.FactorD2 == "Factor 5") {
      this.nombreF2 = "Prácticas Justas";
    } else if (this.FactorD2 == "Factor 6") {
      this.nombreF2 = "Consumidores";
    } else if (this.FactorD2 == "Factor 7") {
      this.nombreF2 = "Participación Activa";
    }


  }
}
