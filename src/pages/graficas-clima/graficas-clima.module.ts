import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GraficasClimaPage } from './graficas-clima';

@NgModule({
  declarations: [
    GraficasClimaPage,
  ],
  imports: [
    IonicPageModule.forChild(GraficasClimaPage),
  ],
})
export class GraficasClimaPageModule {}
