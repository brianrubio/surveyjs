import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultadosDesgastePage } from './resultados-desgaste';

@NgModule({
  declarations: [
    ResultadosDesgastePage,
  ],
  imports: [
    IonicPageModule.forChild(ResultadosDesgastePage),
  ],
})
export class ResultadosDesgastePageModule {}
