import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ResultadosDesgastePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-resultados-desgaste',
  templateUrl: 'resultados-desgaste.html',
})
export class ResultadosDesgastePage {
  result: any;
  sema: number;

  FactorD: any;
  FactorD2: any;

  nombreF1: any;
  nombreF2: any;
   

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.result = this.navParams.get('result');

    this.sema=this.result[5];

    this.FactorD = this.result[6];
    this.FactorD2 = this.result[8];

    if (this.FactorD == "Factor 1") {
      this.nombreF1 = "Agotamiento";
    } else if (this.FactorD == "Factor 2") {
      this.nombreF1 = "Despersonalización";
    } else if (this.FactorD == "Factor 3") {
      this.nombreF1 = "Insatisfacción de logro";
    } 

    if (this.FactorD2 == "Factor 1") {
      this.nombreF2 = "Agotamiento";
    } else if (this.FactorD2 == "Factor 2") {
      this.nombreF2 = "Despersonalización";
    } else if (this.FactorD2 == "Factor 3") {
      this.nombreF2 = "Insatisfacción de logro";
    }

  }

}
