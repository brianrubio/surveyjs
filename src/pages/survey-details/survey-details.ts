import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { SurveyResultsPage } from '../survey-results/survey-results';

@IonicPage()
@Component({
    selector: 'page-survey-details',
    templateUrl: 'survey-details.html',
})
export class SurveyDetailsPage {

    currentYear = new Date().getFullYear();
    survey: any;
    showAlertMessage = false;
    
    constructor(private alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams) {
        this.survey = this.navParams.get('survey');
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad SurveyDetailsPage');
    }
	
	getSurveyResults() {
		this.navCtrl.push(SurveyResultsPage, {
            survey: this.survey
        });
    }

    exit() {
		console.log("Yei");
    }
    
    ionViewCanLeave() {
        if (this.showAlertMessage) {
            let alertPopup = this.alertCtrl.create({
                title: 'Exit',
                message: '¿Are you sure?',
                buttons: [{
                    text: 'Exit',
                    handler: () => {
                        alertPopup.dismiss().then(() => {
                            this.exitPage();
                        });
                    }
                },
                {
                    text: 'Stay',
                    handler: () => {
                        // need to do something if the user stays?
                    }
                }]
            });

            // Show the alert
            alertPopup.present();

            // Return false to avoid the page to be popped up
            return false;
        }
    }

    private exitPage() {
        this.showAlertMessage = false;
        this.navCtrl.pop();
    }

}
