import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { InAppBrowser } from '@ionic-native/in-app-browser';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { IniciarSesionPage } from '../pages/iniciar-sesion/iniciar-sesion';
import { SurveyDetailsPage } from '../pages/survey-details/survey-details';
import { SurveyResultsPage } from '../pages/survey-results/survey-results';

import { SurveyComponent } from '../components/survey/survey';
import { SurveyProvider } from '../providers/survey/survey';
import { ApiWrapper } from '../providers/survey/api-wrapper';
import { ChartComponent } from '../components/chart/chart';

import { TabsAdminPage } from '../pages/tabs-admin/tabs-admin';
import { Home1aPage } from '../pages/home1a/home1a';
import { RegisterPage } from '../pages/register/register';

import { GraficasDesgastePage } from '../pages/graficas-desgaste/graficas-desgaste';
import { GraficasClimaPage } from '../pages/graficas-clima/graficas-clima';
import { GraficasResponsabilidadPage } from '../pages/graficas-responsabilidad/graficas-responsabilidad';
import { GraficasSatisfaccionPage } from '../pages/graficas-satisfaccion/graficas-satisfaccion';


import { ChartsModalPage } from '../modals/charts-modal';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { TimingInterceptor } from '../interceptors/timing-interceptor';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AuthProvider } from '../providers/auth/auth';
import { CerrarSesionPage } from '../pages/cerrar-sesion/cerrar-sesion';


import { Screenshot } from '@ionic-native/screenshot';
import { NativeStorage } from '@ionic-native/native-storage';

export const firebaseConfig = {
  apiKey: "AIzaSyBLOJu18YbUPiOHVDAs3Fo6uiBtSsZ6wEA",
    authDomain: "pymes-fa6ec.firebaseapp.com",
    databaseURL: "https://pymes-fa6ec.firebaseio.com",
    storageBucket: "pymes-fa6ec.appspot.com",
    messagingSenderId: "971821538907"
};

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        IniciarSesionPage,
        SurveyComponent,
        SurveyDetailsPage,
        SurveyResultsPage,
        ChartComponent,
        ChartsModalPage, 
        TabsAdminPage,
         Home1aPage, 
         RegisterPage,
        CerrarSesionPage, 
        GraficasDesgastePage,
        GraficasClimaPage,
        GraficasResponsabilidadPage,
        GraficasSatisfaccionPage, 
        
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        IonicModule.forRoot(MyApp, {
            backButtonText: ''
        }),
        AngularFireModule.initializeApp(firebaseConfig),
        AngularFireDatabaseModule,
        AngularFireAuthModule
        
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        IniciarSesionPage,
        SurveyDetailsPage,
        SurveyResultsPage,
        ChartsModalPage, 
        TabsAdminPage, 
        Home1aPage, 
        RegisterPage,
        CerrarSesionPage,
        GraficasDesgastePage,
        GraficasClimaPage,
        GraficasResponsabilidadPage,
        GraficasSatisfaccionPage, 
    ],
    providers: [
        StatusBar, NativeStorage,
        SplashScreen, Screenshot,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        {provide: HTTP_INTERCEPTORS, useClass: TimingInterceptor, multi: true},
        SurveyProvider,
        ApiWrapper,
        AuthProvider, InAppBrowser
    ]
})
export class AppModule {}
